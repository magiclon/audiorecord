package com.baseeasy.record.play;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.baseeasy.record.AudioRecordUtils;
import com.baseeasy.record.R;

import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;


public class AudioPlayerActivity extends Activity {
    MyJzvdStd myJzvdStd;
    private String filePath;
    private ImageView iv_no;
    private ImageView iv_yes;
    private String play="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        init();
    }

    private void init() {
        filePath = getIntent().getExtras().getString(AudioRecordUtils.EXTRAPATH, "");
        play = getIntent().getExtras().getString(AudioRecordUtils.EXTRAPLAY, "");
        if (TextUtils.isEmpty(filePath)) {
            Toast.makeText(this, "视频路径错误", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        setContentView(R.layout.activity_video_player);
        myJzvdStd = findViewById(R.id.videoPlayer);
        iv_no = findViewById(R.id.iv_no);
        iv_yes = findViewById(R.id.iv_yes);
        myJzvdStd.setUp(filePath
                , "", JzvdStd.NORMAL_ORIENTATION);
        myJzvdStd.startVideo();
        myJzvdStd.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        myJzvdStd.titleTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        myJzvdStd.titleTextView.setVisibility(View.GONE);
        myJzvdStd.fullscreenButton.setVisibility(View.GONE);
        myJzvdStd.backButton.setVisibility(View.VISIBLE);
        myJzvdStd.fullscreenButton.setOnClickListener(null);
        myJzvdStd.posterImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        if(!TextUtils.isEmpty(play)){
            iv_no.setVisibility(View.GONE);
            iv_yes.setVisibility(View.GONE);
        }
        iv_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(AudioRecordUtils.EXTRAPATH, filePath);
                intent.putExtra(AudioRecordUtils.EXTRAPREACTION, AudioRecordUtils.EXTRAPREACTION1);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
        iv_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(AudioRecordUtils.EXTRAPATH, filePath);
                intent.putExtra(AudioRecordUtils.EXTRAPREACTION, AudioRecordUtils.EXTRAPREACTION2);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Jzvd.releaseAllVideos();
    }
}
