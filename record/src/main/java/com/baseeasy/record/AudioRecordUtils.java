package com.baseeasy.record;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;

import com.baseeasy.record.play.AudioPlayerActivity;

public class AudioRecordUtils {
    public static final String EXTRAPATH = "extrapath";
    public static final String EXTRAPREACTION = "preaction";
    public static final int EXTRAPREACTION1 = 1;
    public static final int EXTRAPREACTION2 = 2;
    public static final String EXTRAPLAY = "play";
    public static final String EXTRAFILENAME = "extrafilename";
    public static final String EXTRANEEDPREVIEW = "extraneedpreview";
    public static final String EXTRAMIN = "min";
    public static final String EXTRAMAX = "max";
    public static final int RECORDCODE = 101;
    public static AudioRecorder.RecordLister recordLister;
    private String mPath;
    private String mFileName;
    private boolean needPreView;
    private long mMinDuration = 2;
    private long mMaxDuration = 120;

    public static void start(Activity activity, AudioRecorder.RecordLister recordLister) {
        if (!FastClickUtil.isFastClickActivity(AudioRecordActivity.class.getSimpleName())) {
            startActivityForResult(activity, RECORDCODE, "", "", false, recordLister);
        }
    }

    public static void start(Activity activity, long max,AudioRecorder.RecordLister recordLister) {
        if (!FastClickUtil.isFastClickActivity(AudioRecordActivity.class.getSimpleName())) {
            startActivityForResult(activity, RECORDCODE, "", "", false, 2,max,recordLister);
        }
    }

    public static void startPre(Activity activity, AudioRecorder.RecordLister recordLister) {
        if (!FastClickUtil.isFastClickActivity(AudioRecordActivity.class.getSimpleName())) {
            startActivityForResult(activity, RECORDCODE, "", "", true, recordLister);
        }
    }

    public static void startPre(Activity activity,long max, AudioRecorder.RecordLister recordLister) {
        if (!FastClickUtil.isFastClickActivity(AudioRecordActivity.class.getSimpleName())) {
            startActivityForResult(activity, RECORDCODE, "", "", true,2,max, recordLister);
        }
    }

    public static void start(Activity activity, String path, String filename, AudioRecorder.RecordLister recordLister) {
        if (!FastClickUtil.isFastClickActivity(AudioRecordActivity.class.getSimpleName())) {
            startActivityForResult(activity, RECORDCODE, path, filename, false, recordLister);
        }
    }

    public static void startPre(Activity activity, String path, String filename, AudioRecorder.RecordLister recordLister) {
        if (!FastClickUtil.isFastClickActivity(AudioRecordActivity.class.getSimpleName())) {
            startActivityForResult(activity, RECORDCODE, path, filename, true, recordLister);
        }
    }

    public static void startWithPath(Activity activity, String path, AudioRecorder.RecordLister recordLister) {
        if (!FastClickUtil.isFastClickActivity(AudioRecordActivity.class.getSimpleName())) {
            startActivityForResult(activity, RECORDCODE, path, "", false, recordLister);
        }
    }

    public static void startWithPathPre(Activity activity, String path, AudioRecorder.RecordLister recordLister) {
        if (!FastClickUtil.isFastClickActivity(AudioRecordActivity.class.getSimpleName())) {
            startActivityForResult(activity, RECORDCODE, path, "", true, recordLister);
        }
    }

    public static void startWithPathMax(Activity activity, String path,long max, AudioRecorder.RecordLister recordLister) {
        if (!FastClickUtil.isFastClickActivity(AudioRecordActivity.class.getSimpleName())) {
            startActivityForResult(activity, RECORDCODE, path, "", false,2,max, recordLister);
        }
    }
    public static void startWithPathMaxPre(Activity activity, String path, long max,AudioRecorder.RecordLister recordLister) {
        if (!FastClickUtil.isFastClickActivity(AudioRecordActivity.class.getSimpleName())) {
            startActivityForResult(activity, RECORDCODE, path, "", true,2,max, recordLister);
        }
    }

    public static void startWithFileName(Activity activity, String filename, AudioRecorder.RecordLister recordLister) {
        if (!FastClickUtil.isFastClickActivity(AudioRecordActivity.class.getSimpleName())) {
            startActivityForResult(activity, RECORDCODE, "", filename, false, recordLister);
        }
    }

    public static void startWithFileNamePre(Activity activity, String filename, AudioRecorder.RecordLister recordLister) {
        if (!FastClickUtil.isFastClickActivity(AudioRecordActivity.class.getSimpleName())) {
            startActivityForResult(activity, RECORDCODE, "", filename, true, recordLister);
        }
    }
    public static void startWithFileNameMaxPre(Activity activity, String filename,long max, AudioRecorder.RecordLister recordLister) {
        if (!FastClickUtil.isFastClickActivity(AudioRecordActivity.class.getSimpleName())) {
            startActivityForResult(activity, RECORDCODE, "", filename, true,2,max, recordLister);
        }
    }

    public static void start(Activity activity, AudioRecordUtils.Builder builder, AudioRecorder.RecordLister recordLister) {
        if (!FastClickUtil.isFastClickActivity(AudioRecordActivity.class.getSimpleName())) {
            startActivityForResult(activity, RECORDCODE, builder.build(), recordLister);
        }
    }

    public static void startActivityForResult(Activity activity, int requestCode, String path, String filename, boolean needPreview, AudioRecorder.RecordLister recordLister) {
        Intent intent = new Intent(activity, AudioRecordActivity.class);
        if (!TextUtils.isEmpty(path)) {
            intent.putExtra(EXTRAPATH, path);
        }
        if (!TextUtils.isEmpty(filename)) {
            intent.putExtra(EXTRAFILENAME, filename);
        }
        intent.putExtra(EXTRANEEDPREVIEW, needPreview);
        AudioRecordUtils.recordLister = recordLister;
        activity.startActivityForResult(intent, requestCode);
    }

    public static void startActivityForResult(Activity activity, int requestCode, String path, String filename, boolean needPreview, long min, long max, AudioRecorder.RecordLister recordLister) {
        Intent intent = new Intent(activity, AudioRecordActivity.class);
        if (!TextUtils.isEmpty(path)) {
            intent.putExtra(EXTRAPATH, path);
        }
        if (!TextUtils.isEmpty(filename)) {
            intent.putExtra(EXTRAFILENAME, filename);
        }
        intent.putExtra(EXTRANEEDPREVIEW, needPreview);
        if (min > 0) {
            intent.putExtra(EXTRAMIN, min);
        }
        if (max > 0) {
            intent.putExtra(EXTRAMAX, max);
        }
        AudioRecordUtils.recordLister = recordLister;
        activity.startActivityForResult(intent, requestCode);
    }

    public static void startActivityForResult(Activity activity, int requestCode, AudioRecordUtils audioRecordUtils, AudioRecorder.RecordLister recordLister) {
        Intent intent = new Intent(activity, AudioRecordActivity.class);
        if (!TextUtils.isEmpty(audioRecordUtils.getmPath())) {
            intent.putExtra(EXTRAPATH, audioRecordUtils.getmPath());
        }
        if (!TextUtils.isEmpty(audioRecordUtils.getmFileName())) {
            intent.putExtra(EXTRAFILENAME, audioRecordUtils.getmFileName());
        }
        intent.putExtra(EXTRANEEDPREVIEW, audioRecordUtils.isNeedPreView());
        if (audioRecordUtils.getmMinDuration() > 0) {
            intent.putExtra(EXTRAMIN, audioRecordUtils.getmMinDuration());
        }
        if (audioRecordUtils.getmMaxDuration() > 0) {
            intent.putExtra(EXTRAMAX, audioRecordUtils.getmMaxDuration());
        }
        AudioRecordUtils.recordLister = recordLister;
        activity.startActivityForResult(intent, requestCode);
    }

    public AudioRecordUtils(AudioRecordUtils.Builder builder) {
        this.mPath = builder.mPath;
        this.mFileName = builder.mFileName;
        this.needPreView = builder.needPreView;
        this.mMinDuration = builder.mMinDuration;
        this.mMaxDuration = builder.mMaxDuration;
    }

    public String getmPath() {
        return TextUtils.isEmpty(mPath) ? "" : mPath;
    }

    public void setmPath(String mPath) {
        this.mPath = mPath;
    }

    public String getmFileName() {
        return TextUtils.isEmpty(mFileName) ? "" : mFileName;
    }

    public void setmFileName(String mFileName) {
        this.mFileName = mFileName;
    }

    public boolean isNeedPreView() {
        return needPreView;
    }

    public void setNeedPreView(boolean needPreView) {
        this.needPreView = needPreView;
    }

    public long getmMinDuration() {
        return mMinDuration;
    }

    public void setmMinDuration(long mMinDuration) {
        this.mMinDuration = mMinDuration;
    }

    public long getmMaxDuration() {
        return mMaxDuration;
    }

    public void setmMaxDuration(long mMaxDuration) {
        this.mMaxDuration = mMaxDuration;
    }

    public static void play(Activity activity, String videoPath) {
        Intent intent = new Intent(activity, AudioPlayerActivity.class);
        if (!TextUtils.isEmpty(videoPath)) {
            intent.putExtra(EXTRAPATH, videoPath);
        }
        intent.putExtra(EXTRAPLAY, EXTRAPLAY);
        activity.startActivity(intent);
    }

    public static final class Builder {
        private String mPath;
        private String mFileName;
        private boolean needPreView;
        private long mMinDuration = 2;
        private long mMaxDuration = 120;

        public Builder() {
        }

        public AudioRecordUtils.Builder path(String mPath) {
            this.mPath = mPath;
            return this;
        }

        public AudioRecordUtils.Builder filename(String mFileName) {
            this.mFileName = mFileName;
            return this;
        }

        public AudioRecordUtils.Builder needPreView(boolean needPreView) {
            this.needPreView = needPreView;
            return this;
        }

        public AudioRecordUtils.Builder min(long mMinDuration) {
            this.mMinDuration = mMinDuration;
            return this;
        }

        public AudioRecordUtils.Builder max(long mMaxDuration) {
            this.mMaxDuration = mMaxDuration;
            return this;
        }

        public AudioRecordUtils build() {
            return new AudioRecordUtils(this);
        }
    }
}
